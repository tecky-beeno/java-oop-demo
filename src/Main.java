import game.demo.Animal;
import game.demo.Cat;
import game.core.GameWindow;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");

        Animal animal = new Cat("kitty");
        animal.move();

        GameWindow gameWindow = new GameWindow();
    }
}
