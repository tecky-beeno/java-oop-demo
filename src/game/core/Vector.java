package game.core;

public class Vector {
    public int x;
    public int y;

    @Override
    public Vector clone() {
        Vector result = new Vector();
        result.x = x;
        result.y = y;
        return result;
    }
}
