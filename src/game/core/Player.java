package game.core;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Player extends Shape implements KeyListener {
    GameCanvas canvas;
    Vector speed = new Vector();

    public Player(GameCanvas canvas) {
        super(10, Color.red);
        this.canvas = canvas;
    }


    void updatePosition() {
        position.x += speed.x;
        position.y += speed.y;
        canvas.repaint();
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        updatePosition();
        char keyChar = keyEvent.getKeyChar();
        if (keyChar == ' ') {
            dropItem();
        }
    }

    void dropItem() {
        Bomb bomb = new Bomb();
        // bomb.position = this.position.clone();
        bomb.position.x = this.position.x;
        bomb.position.y = this.position.y;
        canvas.addShape(bomb);
        canvas.repaint();
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyChar()) {
            case 'w':
                speed.y = -1;
                break;
            case 's':
                speed.y = 1;
                break;
            case 'a':
                speed.x = -1;
                break;
            case 'd':
                speed.x = 1;
                break;
        }
        updatePosition();
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyChar()) {
            case 'w':
            case 's':
                speed.y = 0;
                break;
            case 'a':
            case 'd':
                speed.x = 0;
                break;
        }
        updatePosition();
    }
}
