package game.core;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class QuitButton extends Button implements MouseListener {
    GameWindow gameWindow;

    public QuitButton(GameWindow gameWindow) {
        super("quit");
        this.gameWindow = gameWindow;
        this.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        System.out.println("clicked quit button");
        gameWindow.setVisible(false);
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
